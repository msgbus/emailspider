module.exports = {
	//数据库配置
	data_base : {
		db : "coding_data",
		host : "localhost",
		port : 27017
	},
	//请求头
	header : {
		"Host" : "coding.net",
		"User-Agent" : "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0",
		"Accept" : "application/json, text/plain, */*",
		"Accept-Language" : "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3",
		"Accept-Encoding" : "gzip, deflate, br",
		"Referer" : "https://coding.net/projects",
		"Cookie" : "c=coding-cli%3Dfalse%2Cgit-upload%3Dtrue%2Cproject_tweet%3Dtrue%2Csvn%3Dtrue%2Cteam%3Dtrue%2C0e095407; exp=89cd78c2; frontlog_sample_rate=1; _ga=GA1.2.341652799.1468829248; code=coding-cli%3Dfalse%2Cdesktop-notification%3Dfalse%2Cfind_file%3Dfalse%2Cgit-upload%3Dtrue%2Ci18n%3Dfalse%2Clint%3Dfalse%2Cpaas-shuren%3Dfalse%2Cpages-ssl%3Dfalse%2Cproject_tweet%3Dtrue%2Csvn%3Dtrue%2Cteam%3Dtrue%2Ctopic%3Dfalse%2C0f005107; sid=844787af-fec7-4926-b870-218167f66f08; _gat=1; new-year-banner=true",
		"Connection" : "keep-alive"
	},
	//获取主页链接并发连接数
	asyncLink : 50,
	//获取Email并发连接数
	asyncEmail : 20
}