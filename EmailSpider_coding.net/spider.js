var async = require("async");
var eventproxy = require("eventproxy");
var cheerio = require("cheerio");
var request = require("superagent");
var setting = require("./setting.js");
var unique = require("./mini_func/unique.js");
var mongoose = require("mongoose");
var user_model = require("./models/user.js");

mongoose.connect("mongodb://" + setting.data_base.host + ":" + setting.data_base.port + "/" + setting.data_base.db);

var header = setting.header;
var asyncLink = setting.asyncLink;	//获取主页链接并发连接数
var asyncEmail = setting.asyncEmail;	//获取Email并发连接数
var link_list = [];	//存放所有项目主页链接
var EmailList = [];	//存放所抓取Email
var count = 0;	//计数器 
var repeat_count = 0;

function start(){
	get_all_links("https://coding.net/api/public/all?page=1&pageSize=20", link_list);
}

//获取项目列表页数
function get_total_page(link){
	return new Promise((resolve, reject) => {
		request.get(link)
			.set(header)
			.end((error, result) => {
				error ? reject(error) : resolve(result);
			})
	});
}

//获取指定页数所有的项目主页链接
function get_page_links(query){
	return new Promise((resolve, reject) => {
		request.get("https://coding.net/api/public/all?page=" + query + "&pageSize=20")
			.set(header)
			.end((error, result) => {
				error ? reject(error) : resolve(result);
			})
	});
}

//获取项目主页
/*
https://coding.net/api + link + /git/list_branches
https://coding.net/api + link + /git/tree/master
*/
function get_project_page(link){
	return new Promise((resolve, reject) => {
			request.get("https://coding.net/api" + link + "/git/tree/master")
				.set(header)
				.end((error, result) => {
					error ? reject(error) : resolve(result);
				})
		});
}

//持久化存储
function saveEmails(result){
	var users = [];
	//把Email为空的用户剔除
	for(var i=0; i<result.length; i++){
		if(result[i].email != ""){
			users.push(result[i]);
		};
	};
	//把Email重复的数据剔除
	users = unique(users);
	console.log("可利用数据一共有：" + users.length);

	//写入数据库
	count = 0;
	repeat_count = 0;
	for(let i = 0; i < users.length; i++){
		user_model.findOne({ email : users[i].email }, function(err, doc){
			if(err){
				console.log(err);
			};
			if(!doc){
				user_model.create({ link : users[i].link, name : users[i].name, email : users[i].email }, function(err, doc){
					if(err){
						console.log(err);
					}else{
						count++;
						console.log("已成功保存 " + count + " 条记录");
					};
				});
			}else{
				repeat_count++;
				console.log(repeat_count + " 条记录重复");
			};
		});
	};
}

//获取所有的项目主页链接
function get_all_links(link, link_list){
	get_total_page(link)
		.then((result) => {
			// var pages = result.body.data.totalPage;
			// 3750
			var pages = 3700;
			//查询参数数组
			var queryList = [];
			console.log("列表页数：" + pages);
			for(var query = 1; query < pages + 1; query++){
				queryList.push(query);
			};
			console.log("即将异步并发抓取所有项目主页链接，当前并发数为：" + asyncLink);
			async.mapLimit(queryList, asyncLink, function(query, callback){
				console.log("正在抓取 page = "+ query + " 的项目列表");
				get_page_links(query)
					.then((result) => { 
						//当前所在页所有项目集合
						var project_list = result.body.data.list;
						//当前所在页所有跟随者或关注者主页链接
						for(var i = 0; i < project_list.length; i ++){
							if(project_list[i].backend_project_path){
								link_list.push(project_list[i].backend_project_path);
							};
						};
						console.log("已成功抓取 " + link_list.length + " 条项目主页链接");
						callback(null, "");
					})
					.catch((error) => {
						callback(null, "");
						console.log(error);
					});
			},function(error, result){
				if(error){
					console.log(error);
				}else{
					console.log("所有项目主页链接已抓取完毕,一共有 " + link_list.length + " 条链接：");
					console.log(link_list);
					//根据项目主页链接并发抓取相对应的Email
					getEmails(link_list);
				};
			});
		})
		.catch((error) => {
			console.log(error);
		});
}

//获取所有Email
function getEmails(link_list){
	console.log("即将异步并发抓取所有项目主页Email，当前并发数为：" + asyncEmail);
	async.mapLimit(link_list, asyncEmail, function(link, callback){
			get_project_page(link)
				.then((result) => {
					count++;
					var email = "";
					console.log("正在抓取第 " + count + " 个用户的Email");
					if(result.body.data.lastCommitter && result.body.data.lastCommitter.email){
						email = result.body.data.lastCommitter.email;
						name = result.body.data.lastCommitter.name;
						console.log(name + ": " + email);
					};
					callback(null, { link : link, name : name, email : email });
				})
				.catch((error) => {
					callback(null, { link : link, name : "", email : "" });
					console.log(error);
				});
			},function(error, result){
				if(error){
					console.log(error);
				}else{
					console.log("all right");
					console.log(result);
					console.log("正在对数据进行存储...");
					saveEmails(result);
				};
	});
}

start();